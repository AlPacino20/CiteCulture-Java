/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;


public class Fidelite  {

    private Integer idFidelite;

    private String libelle;
   
    private String description;

    private String type;
 
    private String status;
 
    private Date dateobtenation;

    private Integer points;
   
    private String image;
   
    private Integer fosUserId;

    public Fidelite() {
    }

    
    public Fidelite(Integer idFidelite) {
        this.idFidelite = idFidelite;
    }

 
    public Fidelite(Integer idFidelite,Integer fosUserId, String libelle, String description, String type, String status, Date dateobtenation, Integer points) {
        this.idFidelite = idFidelite;
           this.fosUserId = fosUserId;
        this.libelle = libelle;
        this.description = description;
        this.type = type;
        this.status = status;
        this.dateobtenation = dateobtenation;
        this.points = points;
    }

 

    public Integer getIdFidelite() {
        return idFidelite;
    }

    public void setIdFidelite(Integer idFidelite) {
        this.idFidelite = idFidelite;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateobtenation() {
        return dateobtenation;
    }

    public void setDateobtenation(Date dateobtenation) {
        this.dateobtenation = dateobtenation;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public Integer getFosUserId() {
        return fosUserId;
    }

    public void setFosUserId(Integer fosUserId) {
        this.fosUserId = fosUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFidelite != null ? idFidelite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fidelite)) {
            return false;
        }
        Fidelite other = (Fidelite) object;
        if ((this.idFidelite == null && other.idFidelite != null) || (this.idFidelite != null && !this.idFidelite.equals(other.idFidelite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Fidelite[ idFidelite=" + idFidelite + " ]";
    }
    
}
