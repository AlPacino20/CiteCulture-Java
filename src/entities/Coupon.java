/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;



public class Coupon  {

 
 
    private Integer idCoupon;

    private String libelle;
 
    private String description;
 
    private String status;
    
    private String typec;

    private Date dateceation;
 
    private Date periodevalidite;
  
    private Integer fos_user_id;

 

    public Coupon() {
    }

    public Coupon(Integer idCoupon) {
        this.idCoupon = idCoupon;
    }

    public Coupon(int idCoupon, String libelle, String description, String status,  String typec, Date dateceation, Date periodevalidite,int fos_user_id) {
        this.idCoupon = idCoupon;
        this.libelle = libelle;
        this.description = description;
        this.status = status;
        this.typec=typec;
        this.dateceation = dateceation;
        this.periodevalidite = periodevalidite;
        this.fos_user_id = fos_user_id;
    }

  

    public Integer getIdCoupon() {
        return idCoupon;
    }

    public void setIdCoupon(Integer idCoupon) {
        this.idCoupon = idCoupon;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTypec() {
        return typec;
    }

    public void setTypec(String typec) {
        this.typec = typec;
    }

    public Date getDateceation() {
        return dateceation;
    }

    public void setDateceation(Date dateceation) {
        this.dateceation = dateceation;
    }

    public Date getPeriodevalidite() {
        return periodevalidite;
    }

    public void setPeriodevalidite(Date periodevalidite) {
        this.periodevalidite = periodevalidite;
    }

    public Integer getFosUserId() {
        return fos_user_id;
    }

    public void setFosUserId(Integer fosUserId) {
        this.fos_user_id = fosUserId;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCoupon != null ? idCoupon.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Coupon)) {
            return false;
        }
        Coupon other = (Coupon) object;
        if ((this.idCoupon == null && other.idCoupon != null) || (this.idCoupon != null && !this.idCoupon.equals(other.idCoupon))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Coupon[ idCoupon=" + idCoupon + " ]";
    }
    
}
