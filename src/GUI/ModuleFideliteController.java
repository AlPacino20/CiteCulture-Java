/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import entities.Coupon;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import services.CouponService;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener.Change;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javax.swing.JOptionPane;
import utilities.ControleSaisie;

/**
 * FXML Controller class
 *
 * @author AlPacino20
 */
public class ModuleFideliteController implements Initializable {

    /**
     * Initializes the controller class.
     */
    //Liste
    @FXML
    private TableView<Coupon> CouponTable;
    @FXML
    private TableColumn<Coupon, Integer> id_coupon;
    @FXML
    private TableColumn<Coupon, Integer> fos_user_id;
    @FXML
    private TableColumn<Coupon, String> libelle;
    @FXML
    private TableColumn<Coupon, String> status;
    @FXML
    private TableColumn<Coupon, String> typec;
    @FXML
    private TableColumn<Coupon, String> description;
    @FXML
    private TableColumn<Coupon, LocalDate> date_ob;
    @FXML
    private TableColumn<Coupon, LocalDate> date_lim;
    @FXML
    private Label lbl_idcoupon;
    @FXML
    private Label lbl_fos_user_id;
    @FXML
    private Label lbl_libelle;
    @FXML
    private Label lbl_status;
    @FXML
    private Label lbl_typec;
    @FXML
    private JFXTextArea lbl_description;
    @FXML
    private Label lbl_date_ob;
    @FXML
    private Label lbl_date_lim;
    //rechercher
    @FXML
    private TextField recherche;
    //supprimer
    @FXML
    private JFXButton btn_suppcoupon;
    @FXML
    private JFXButton btn_modifcoupon;
    //Modifier
    @FXML
    private JFXButton btn_enrgcoupon;
    @FXML
    private JFXTabPane tab_coupon;
    @FXML
    private JFXTextField txt_idcoupon;
    @FXML
    private JFXTextField txt_libelle;
    @FXML
    private JFXTextField txt_cuser;
    @FXML
    private JFXDatePicker txt_dobt;
    @FXML
    private JFXDatePicker txt_dval;
    @FXML
    private JFXTextArea txt_desc;
    @FXML
    ToggleGroup statusradioGroup;
    @FXML
    private JFXRadioButton status_actif;
    @FXML
    private JFXRadioButton status_inactif;
    @FXML
    private JFXButton btn_anncoupon;
    @FXML
    ToggleGroup typeradioGroup;
    @FXML
    private JFXRadioButton type_happy;
    @FXML
    private JFXRadioButton type_premuim;
    @FXML
    private JFXRadioButton type_standard;

    //Ajouter
    @FXML
    private JFXButton btn_enrgcoupon1;
    @FXML
    private JFXTextField txt_idcoupon1;
    @FXML
    private JFXTextField txt_libelle1;
    @FXML
    private JFXTextField txt_cuser1;
    @FXML
    private JFXDatePicker txt_dobt1;
    @FXML
    private JFXDatePicker txt_dval1;
    @FXML
    private JFXTextArea txt_desc1;
    @FXML
    ToggleGroup statusradioGroup1;
    @FXML
    ToggleGroup typeradioGroup1;
    @FXML
    private JFXRadioButton status_actif1;
    @FXML
    private JFXRadioButton status_inactif1;
    @FXML
    private JFXRadioButton type_happy1;
    @FXML
    private JFXRadioButton type_premuim1;
    @FXML
    private JFXRadioButton type_standard1;
    @FXML
    private JFXButton btn_anncoupon1;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //initiliation de la la liste des couons et affichage

        CouponService cs = new CouponService();
        ControleSaisie controle = new ControleSaisie();

        lbl_description.setStyle("-fx-background-color: gray; -fx-text-fill: white;");
        id_coupon.setCellValueFactory(new PropertyValueFactory<>("idCoupon"));
        fos_user_id.setCellValueFactory(new PropertyValueFactory<>("fos_user_id"));
        libelle.setCellValueFactory(new PropertyValueFactory<>("libelle"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));
        typec.setCellValueFactory(new PropertyValueFactory<>("typec"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        date_ob.setCellValueFactory(new PropertyValueFactory<>("dateceation"));
        date_lim.setCellValueFactory(new PropertyValueFactory<>("periodevalidite"));

        ObservableList<Coupon> obListCoupon = cs.afficherCoupons();
        CouponTable.setItems(obListCoupon);

        SingleSelectionModel<Tab> selectionModel = tab_coupon.getSelectionModel();
        selectionModel.select(2);

        ObservableList selectedCells = CouponTable.getSelectionModel().getSelectedCells();

        selectedCells.addListener((Change c) -> {
            Coupon selectedCoupon = CouponTable.getSelectionModel().getSelectedItem();
            lbl_idcoupon.setText(selectedCoupon.getIdCoupon().toString());
            lbl_fos_user_id.setText(selectedCoupon.getFosUserId().toString());
            lbl_libelle.setText(selectedCoupon.getLibelle());
            lbl_status.setText(selectedCoupon.getStatus());

            lbl_description.clear();
            lbl_description.appendText(selectedCoupon.getDescription());

            lbl_date_ob.setText(selectedCoupon.getDateceation().toString());
            lbl_date_lim.setText(selectedCoupon.getPeriodevalidite().toString());
            lbl_typec.setText(selectedCoupon.getTypec());
            btn_suppcoupon.setDisable(false);
            btn_modifcoupon.setDisable(false);
        });

        //rechercher coupon fonction
        ObservableList data = CouponTable.getItems();
        recherche.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (oldValue != null && (newValue.length() < oldValue.length())) {
                CouponTable.setItems(data);
            }
            String value = newValue.toLowerCase();
            ObservableList<Coupon> subentries = FXCollections.observableArrayList();

            long count = CouponTable.getColumns().stream().count();
            for (int i = 0; i < CouponTable.getItems().size(); i++) {
                for (int j = 0; j < count; j++) {
                    String entry = "" + CouponTable.getColumns().get(j).getCellData(i);
                    if (entry.toLowerCase().contains(value)) {
                        subentries.add(CouponTable.getItems().get(i));
                        break;
                    }
                }
            }
            CouponTable.setItems(subentries);
        });

        //supprimer coupon fonction
        btn_suppcoupon.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            Coupon selectedCoupon = CouponTable.getSelectionModel().getSelectedItem();
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "êtes-vous sûr de vouloir supprimer ce coupon ? ", "Suppression", dialogButton);
            if (dialogResult == 0) {
                cs.supprimerCoupon(selectedCoupon.getIdCoupon());
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Suppression");
                alert.setContentText("Votre coupon du code n°: " + selectedCoupon.getIdCoupon() + " a été suprimier avec succès");
                alert.showAndWait();

                lbl_idcoupon.setText("");
                lbl_fos_user_id.setText("");
                lbl_libelle.setText("");
                lbl_status.setText("");

                lbl_description.setText("");
                lbl_description.setText("");

                lbl_date_ob.setText("");
                lbl_date_lim.setText("");

                txt_idcoupon.setText("");
                txt_libelle.setText("");
                txt_cuser.setText("");

                status_actif.setSelected(false);

                status_inactif.setSelected(false);

                type_happy.setSelected(false);
                type_premuim.setSelected(false);
                type_standard.setSelected(false);

                txt_dobt.getEditor().clear();

                txt_dval.getEditor().clear();

                txt_desc.setText("");

                CouponTable.refresh();
                CouponTable.getItems().clear();
                ObservableList<Coupon> obListCoupon1 = cs.afficherCoupons();
                CouponTable.setItems(obListCoupon1);
            }
        });
        //modifier btn coupon fonction
        btn_modifcoupon.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            Coupon selectedCoupon = CouponTable.getSelectionModel().getSelectedItem();
            selectionModel.select(1);
            
            //selectionModel.clearSelection(); //clear your selection
            txt_idcoupon.setText(selectedCoupon.getIdCoupon().toString());
            txt_libelle.setText(selectedCoupon.getLibelle());
            txt_cuser.setText(selectedCoupon.getFosUserId().toString());

            if (selectedCoupon.getStatus().equals("Actif")) {
                status_actif.setSelected(true);
            } else {
                status_inactif.setSelected(true);
            }

            if (selectedCoupon.getTypec().equals("Happy")) {
                type_happy.setSelected(true);
            } else if (selectedCoupon.getTypec().equals("Premuim")) {
                type_premuim.setSelected(true);

            } else {
                type_standard.setSelected(true);
            }

            txt_dobt.setValue(Instant.ofEpochMilli(selectedCoupon.getDateceation().getTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate());

            txt_dval.setValue(Instant.ofEpochMilli(selectedCoupon.getPeriodevalidite().getTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate());

            txt_desc.setText(selectedCoupon.getDescription());
        });

        //enregstrer modifier coupon fonction
        btn_enrgcoupon.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            Coupon selectedCoupon = CouponTable.getSelectionModel().getSelectedItem();
            RadioButton sl = (RadioButton) statusradioGroup.getSelectedToggle();
            RadioButton tl = (RadioButton) typeradioGroup.getSelectedToggle();

            if (!controle.controleCodeCoupon(txt_idcoupon) && !controle.controleLibelleCoupon(txt_libelle) && !controle.controleIDPropritaireCoupon(txt_cuser) && !controle.controleDateCoupon(txt_dobt, txt_dval) && !controle.controleDescriptionCoupon(txt_desc)) {

                Coupon cpn = new Coupon(Integer.parseInt(txt_idcoupon.getText()),
                        txt_libelle.getText(),
                        txt_desc.getText(),
                        sl.getText(),
                        tl.getText(),
                        java.sql.Date.valueOf(txt_dobt.getValue()),
                        java.sql.Date.valueOf(txt_dval.getValue()),
                        Integer.parseInt(txt_cuser.getText())
                );

                cs.modifierCoupons(selectedCoupon.getIdCoupon(), cpn);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Modification");
                alert.setContentText("Votre coupon du code n°: " + selectedCoupon.getIdCoupon() + " a été modifier avec succès");
                alert.showAndWait();

                CouponTable.refresh();
                CouponTable.getItems().clear();
                ObservableList<Coupon> obListCoupon1 = cs.afficherCoupons();
                CouponTable.setItems(obListCoupon1);

                selectionModel.select(2);
            }
        });
        //modifier & ajout tab coupon fonction
        tab_coupon.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {

            Coupon selectedCoupon = CouponTable.getSelectionModel().getSelectedItem();
            if (selectionModel.getSelectedIndex() == 1) {
                txt_idcoupon.setText(selectedCoupon.getIdCoupon().toString());
                txt_libelle.setText(selectedCoupon.getLibelle());
                txt_cuser.setText(selectedCoupon.getFosUserId().toString());

                if (selectedCoupon.getStatus().equals("Actif")) {
                    status_actif.setSelected(true);
                } else {
                    status_inactif.setSelected(true);
                }

                if (selectedCoupon.getTypec().equals("Happy")) {
                    type_happy.setSelected(true);
                } else if (selectedCoupon.getTypec().equals("Premuim")) {
                    type_premuim.setSelected(true);

                } else {
                    type_standard.setSelected(true);

                }
                System.out.println("TEST " + typeradioGroup.getSelectedToggle());
                txt_dobt.setValue(Instant.ofEpochMilli(selectedCoupon.getDateceation().getTime())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate());

                txt_dval.setValue(Instant.ofEpochMilli(selectedCoupon.getPeriodevalidite().getTime())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate());

                txt_desc.setText(selectedCoupon.getDescription());

            } else  if (selectionModel.getSelectedIndex() == 0) {
            
             txt_idcoupon1.clear();
            txt_libelle1.clear();
            txt_desc1.clear();
            status_actif1.setSelected(false);
            status_inactif1.setSelected(false);
            type_happy1.setSelected(false);
            type_premuim1.setSelected(false);
            type_standard1.setSelected(false);
            txt_dobt1.getEditor().clear();
            txt_dval1.getEditor().clear();
            txt_cuser1.clear();
            
            }
        });
        
        //annuler btn modif pan coupon fonction
        btn_anncoupon.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            selectionModel.select(2);

        });
        //ajouter coupon fonction
        btn_enrgcoupon1.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            RadioButton sl = (RadioButton) statusradioGroup1.getSelectedToggle();
            RadioButton tl = (RadioButton) typeradioGroup1.getSelectedToggle();

            if (!controle.controleCodeCoupon(txt_idcoupon1) && !controle.controleLibelleCoupon(txt_libelle1) && !controle.controleIDPropritaireCoupon(txt_cuser1) && !controle.controleDateCoupon(txt_dobt1, txt_dval1) && !controle.controleDescriptionCoupon(txt_desc1)) {
                Coupon cpn = new Coupon(Integer.parseInt(txt_idcoupon1.getText()),
                        txt_libelle1.getText(),
                        txt_desc1.getText(),
                        sl.getText(),
                        tl.getText(),
                        java.sql.Date.valueOf(txt_dobt1.getValue()),
                        java.sql.Date.valueOf(txt_dval1.getValue()),
                        Integer.parseInt(txt_cuser1.getText())
                );

                cs.ajouterCoupons(cpn);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Ajouter");
                alert.setContentText("Votre coupon du code n°: " + cpn.getIdCoupon() + " a été ajouter avec succès");
                alert.showAndWait();

                CouponTable.refresh();
                CouponTable.getItems().clear();
                ObservableList<Coupon> obListCoupon1 = cs.afficherCoupons();
                CouponTable.setItems(obListCoupon1);

                selectionModel.select(2);
            }
        });

        //annuler btn ajout pan coupon fonction
        btn_anncoupon1.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {

            txt_idcoupon1.clear();
            txt_libelle1.clear();
            txt_desc1.clear();
            status_actif1.setSelected(false);
            status_inactif1.setSelected(false);
            type_happy1.setSelected(false);
            type_premuim1.setSelected(false);
            type_standard1.setSelected(false);
            txt_dobt1.getEditor().clear();
            txt_dval1.getEditor().clear();
            txt_cuser1.clear();

            selectionModel.select(2);

        });

        
    }
}
