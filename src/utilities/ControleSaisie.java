/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
//import com.soukelmdina.entite.Boutique;
//import com.soukelmdina.entite.Categorie;
import javafx.scene.control.Label;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author Moslah Yassine
 */
public class ControleSaisie {

    public ControleSaisie() {
    }

    
    
    
     public boolean controleDescriptionCoupon(JFXTextArea tArea) {
        String chaine = tArea.getText().trim();
        if (chaine.length() == 0) {
            showMessageDialog(null, "Description vide", "Description", ERROR_MESSAGE);
            tArea.clear();
            return true;
        }
        return false;
    }
    
    //Libelle
    public boolean controleLibelleCoupon(JFXTextField textField) {
        
         String chaine = textField.getText().trim();
        if (chaine.length() == 0) {
          showMessageDialog(null, "Libelle vide", "Libelle", ERROR_MESSAGE);
          textField.clear();  
 
            return true;
        }
        else if (!textField.getText().matches(".*[a-zA-Z].*")) {
          showMessageDialog(null, "Libelle inncorrecte", "Libelle", ERROR_MESSAGE);
          textField.clear();  

            return true;
        }
        return false;
    }
    
    
    //ID propritaire
        public boolean controleIDPropritaireCoupon(JFXTextField textField) {
        String chaine = textField.getText();
        char[] tab = chaine.toCharArray();

        boolean estUnNombre = true;
        
       if (chaine.length() == 0) {
          showMessageDialog(null, "ID Propritaire vide", "ID Propritaire", ERROR_MESSAGE);
          textField.clear();  
 
            return true;
        }
        
        for (int i = 0; i < tab.length; i++) {
            if (!Character.isDigit(tab[i])) {
                estUnNombre = false;
            }
        }
        if (!estUnNombre) {
          showMessageDialog(null, "ID Propritaire inncorrecte", "ID Propritaire", ERROR_MESSAGE);
          textField.clear();  
            return true;
        }
        return false;
    }
        
        
    //Code Coupon
    public boolean controleCodeCoupon(JFXTextField textField) {
        String chaine = textField.getText();
        char[] tab = chaine.toCharArray();
        boolean estUnNombre = true;
        
          if (chaine.length() == 0) {
             showMessageDialog(null, "Code vide", "Code", ERROR_MESSAGE);
            textField.clear();
            return true;
        }
        
        for (int i = 0; i < tab.length; i++) {
            if (!Character.isDigit(tab[i])) {
                estUnNombre = false;
            }      
        
        if (textField.getText().length() != 7  || !estUnNombre) {
            showMessageDialog(null, "Code inncorrecte", "Code", ERROR_MESSAGE);
             textField.clear(); 
            return true;
        } else if (textField.getText().equals("0000000")) {
            showMessageDialog(null, "Code inncorrecte", "Code", ERROR_MESSAGE);
             textField.clear();            
            return true;
        }
        
    }
        return false;
}
        
   //Date Creation et validation
    public boolean controleDateCoupon(JFXDatePicker datedeabut,JFXDatePicker dateFin) {
      
        if (datedeabut.getValue().getDayOfMonth() > dateFin.getValue().getDayOfMonth()) {
      
            datedeabut.getEditor().clear();
            dateFin.getEditor().clear();   
            showMessageDialog(null, "Date du creation doit etre inferieur que la date du validite", "Date", ERROR_MESSAGE);
            return true;
        } else if (datedeabut.getValue().getMonthValue() > dateFin.getValue().getMonthValue()) {
                 
            datedeabut.getEditor().clear();
            dateFin.getEditor().clear();   
            showMessageDialog(null, "Date du creation doit etre inferieur que la date du validite", "Date", ERROR_MESSAGE);
             return true;
                } 
       
        return false;
}
        
      public boolean controleTextFieldVide(JFXTextField textField, String msg, Label errorLabel) {
        String chaine = textField.getText().trim();
        if (chaine.length() == 0) {
            errorLabel.setText(msg);
            textField.clear();
            return true;
        }
        return false;
    }

    public boolean controleMDPVide(JFXPasswordField textField, String msg, Label errorLabel) {
        String chaine = textField.getText().trim();
        if (chaine.length() == 0) {
            errorLabel.setText(msg);
            textField.clear();
            return true;
        }
        return false;
    }

    public void effacerControleSaisie(Label textField) {
        textField.setText("");
    }
   
        

    public boolean controleTextFieldOnlyLetters(JFXTextField textField, String msg, Label errorLabel) {
        String chaine = textField.getText();
        char[] tab = chaine.toCharArray();

        boolean valide = true;

        for (int i = 0; i < tab.length; i++) {
            if (Character.isDigit(tab[i]) || tab[i] == '.' || tab[i] == ',' || tab[i] == '-' || tab[i] == '_' || tab[i] == '@') {
                valide = false;
            }
        }

        if (!valide) {
            errorLabel.setText(msg);
             textField.setPromptText(msg);
            return true;
        }
        return false;
    }

    public boolean controleTextFieldChiffres(JFXTextField textField) {
        String chaine = textField.getText();
        char[] tab = chaine.toCharArray();

        boolean estUnNombre = true;
        for (int i = 0; i < tab.length; i++) {
            if (!Character.isDigit(tab[i])) {
                estUnNombre = false;
            }
        }
        if (!estUnNombre) {
          showMessageDialog(null, "Libelle inncorrecte", "Libelle", ERROR_MESSAGE);
          textField.clear();  
            return true;
        }
        return false;
    }

    
    public boolean controleCPLongueur(JFXTextField textField, String msg, Label errorLabel) {

        if (textField.getText().length() != 4) {
            errorLabel.setText(msg);
             textField.setPromptText(msg);
            return true;
        } else if (textField.getText().equals("0000")) {
            errorLabel.setText("Code postal incorrecte                 ");
             textField.setPromptText(msg);
            return true;
        }
        return false;
    }

    public boolean controleNumTelLongueur(JFXTextField textField, String msg, Label errorLabel) {
        if (textField.getText().length() != 8) {
            errorLabel.setText(msg);
             textField.setPromptText(msg);
            return true;
        } else if (textField.getText().substring(0, 1) != "31" && textField.getText().charAt(0) != '2' && textField.getText().charAt(0) != '5' && textField.getText().charAt(0) != '9' && textField.getText().charAt(0) != '7') {
            errorLabel.setText("N° Tel. incorrecte                 ");
             textField.setPromptText(msg);
            return true;
        }
        return false;
    }

    public boolean controleMailFormat(JFXTextField textField, String msg, Label errorLabel) {
        String chaine = textField.getText();
        if (chaine.length() != 0) {
            if (chaine.charAt(chaine.length() - 1) == '.') {
                errorLabel.setText(msg);
                textField.setPromptText(msg);
                return true;
            } else {

                int firstIndexA = chaine.indexOf("@");
                int lastIndexA = chaine.lastIndexOf("@");
                int lastIndexPt = chaine.lastIndexOf(".");
                if (firstIndexA < 3 || firstIndexA != lastIndexA || firstIndexA > lastIndexPt || lastIndexPt - firstIndexA < 4 || chaine.substring(lastIndexPt + 1, chaine.length() - 1).length() > 3 || chaine.substring(lastIndexPt + 1, chaine.length()).length() < 2) {
                    errorLabel.setText(msg);
                    textField.setPromptText(msg);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean controleComplexiteMDP(JFXPasswordField textField, String msg, Label errorLabel) {
        String chaine = textField.getText();
        char[] tab = chaine.toCharArray();
        boolean chiffre = false;
        boolean minus = false;
        boolean majus = false;

        for (int i = 0; i < tab.length; i++) {
            if (Character.isDigit(tab[i])) {
                chiffre = true;
            } else if (tab[i] >= 'a' && tab[i] <= 'z') {
                minus = true;
            } else if (tab[i] >= 'A' && tab[i] <= 'Z') {
                majus = true;
            }
        }

        if (chaine.length() < 8) {
            errorLabel.setText("Longueur minimal est 8 caractères                  ");
            return true;
        } else if (!(chiffre && minus && majus)) {
            errorLabel.setText(msg);
             textField.setPromptText(msg);
            return true;
        }
        return false;
    }

    public boolean controleComboBox(JFXComboBox<String> combo, String msg, Label errorLabel) {
        if (combo.getValue() == null) {
            errorLabel.setText(msg);
            
            return true;
        }
        return false;
    }

    public boolean controleTextAreaVide(JFXTextArea tArea, String msg, Label errorLabel) {
        String chaine = tArea.getText().trim();
        if (chaine.length() == 0) {
            errorLabel.setText(msg);
            tArea.clear();
            return true;
        }
        return false;
    }

    public boolean controleTextAreaNonNumerique(JFXTextArea tArea, String msg, Label errorLabel) {
        if (!tArea.getText().matches(".*[a-zA-Z].*")) {
            errorLabel.setText(msg);

            return true;
        }
        return false;
    }

    public boolean controleLabelImage(Label path, String msg, Label errorLabel) {
        if (path.getText().equals("Sélectionner une image")) {
            errorLabel.setText(msg);
            return true;
        }
        return false;
    }

    public boolean controleGPS(double latitude, double longitude, String msg, Label errorLabel) {
        if (latitude == 0 && longitude == 0) {
            errorLabel.setText(msg);
            return true;
        }
        return false;
    }

    public boolean controleTextFieldNumerique1(JFXTextField textField, String msg, Label errorLabel) {
        if (!textField.getText().matches(".*[a-zA-Z].*")) {
            errorLabel.setText(msg);

            return true;
        }
        effacerControleSaisie(errorLabel);

        return false;
    }

    public boolean controleNumTelLongueur1(JFXTextField textField, String msg, Label errorLabel) {
        if (textField.getText().length() != 8) {
            errorLabel.setText(msg);
            textField.setPromptText(msg);
            return true;
        } else if (textField.getText().substring(0, 1) != "31" && textField.getText().charAt(0) != '2' && textField.getText().charAt(0) != '5' && textField.getText().charAt(0) != '9' && textField.getText().charAt(0) != '7') {
            errorLabel.setText("N° Tel. incorrecte                 ");
            return true;
        }
        effacerControleSaisie(errorLabel);
        return false;
    }

    public boolean controleTextFieldVide1(JFXTextField textField, String msg, Label errorLabel) {
        String chaine = textField.getText().trim();
        if (chaine.length() == 0) {
            errorLabel.setText(msg);
            textField.clear();
            return true;
        }
        effacerControleSaisie(errorLabel);
        return false;

    }

    public boolean controleTextFieldNonNumerique1(JFXTextField textField, String msg, Label errorLabel) {
        if (textField.getText().matches(".*[a-zA-Z].*")) {
            errorLabel.setText(msg);
            textField.setPromptText(msg);
            return true;
        }
        effacerControleSaisie(errorLabel);

        return false;
    }

    public boolean controleTextAreaVide1(JFXTextArea tArea, String msg, Label errorLabel) {
        String chaine = tArea.getText().trim();
        if (chaine.length() == 0) {
            errorLabel.setText(msg);
            tArea.clear();
            return true;
        }
        effacerControleSaisie(errorLabel);
        return false;
    }

    public boolean controleTextAreaNonNumerique1(JFXTextArea tArea, String msg, Label errorLabel) {
        if (!tArea.getText().matches(".*[a-zA-Z].*")) {
            errorLabel.setText(msg);

            return true;
        }
        effacerControleSaisie(errorLabel);

        return false;
    }

    public boolean controleLabelImage1(Label path, String msg, Label errorLabel) {
        if (path.getText().equals("Sélectionner une image")) {
            errorLabel.setText(msg);
            return true;
        }
        effacerControleSaisie(errorLabel);

        return false;
    }

    public boolean controleTextFieldPrix(JFXTextField textField, String msg, Label errorLabel) {
        String chaine = textField.getText();
        char[] tab = chaine.toCharArray();

        boolean estUnNombre = true;
        for (int i = 0; i < tab.length; i++) {
            if (!Character.isDigit(tab[i]) && tab[i] != '.') {
                estUnNombre = false;
            }
        }
        if (!estUnNombre) {
            errorLabel.setText(msg);
            return true;
        }
        return false;
    }

    public boolean controleComboBoxstring(JFXComboBox<String> combo, String msg, Label errorLabel) {
        if (combo.getValue() == null) {
            errorLabel.setText(msg);
            return true;
        }
        return false;
    }
    
    public boolean controleCPLongueur1(JFXTextField textField, String msg, Label errorLabel) {
        if (textField.getText().length() != 4) {
            errorLabel.setText(msg);
            return true;
        } else if (textField.getText().equals("0000")) {
            errorLabel.setText("Code postal incorrecte");
            return true;
        }
                errorLabel.setText("");
        return false;
    }
    
     public boolean isValidFloat(JFXTextField textField, String msg, Label errorLabel) {
		try {
			Double.parseDouble(textField.getText());
                      
		} catch (NumberFormatException nfe) {
		    errorLabel.setText(msg);	
                    return false;
                         
		}
		return true;		
	}
}
