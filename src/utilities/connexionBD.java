/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Aziz
 */
public class connexionBD {

    static String url = "jdbc:mysql://127.0.0.1:3306/citeculturebd";
    static String name = "root";
    static String password = "";
    static Connection conn;
    static connexionBD instance;

    private connexionBD() {
        try {
            conn = DriverManager.getConnection(url, name, password);
            System.out.println("Connexion établie");
        } catch (SQLException ex) {
            System.out.println("Erreur " + ex.getMessage());
        }
    }

    public Connection getConn() {
        return conn;
    }

    public static connexionBD getInstance() {
        if (instance == null) {
            instance = new connexionBD();
        }
        return instance;
    }

}
