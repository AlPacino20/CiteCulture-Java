/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Coupon;
import utilities.connexionBD;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author AlPacino20
 */
public class CouponService {

    Connection con = connexionBD.getInstance().getConn();

    //create
    public void ajouterCoupons(Coupon C) {
        try {
            Statement st = con.createStatement();
              String req="INSERT INTO  coupon(`id_coupon`, `fos_user_id`, `libelle`,`description`,`status`, `typec` ,`dateceation`,`periodevalidite`)  values('"+C.getIdCoupon()+"','"+C.getFosUserId()+"','"+C.getLibelle()+"','"+C.getDescription()+"','"+C.getStatus()+"','"+C.getTypec()+"','"+C.getDateceation()+"','"+C.getPeriodevalidite()+"')";
            st.executeUpdate(req);
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }
    }


//modifier
    public void modifierCoupons(int id_coupon, Coupon Cpn) {
        try {
            PreparedStatement st = con.prepareStatement("update coupon set id_coupon=? , fos_user_id=? , libelle=? , description=? , status=? , typec=? , dateceation=? , periodevalidite=? where id_coupon=?");

            st.setInt(1, Cpn.getIdCoupon());
            st.setInt(2, Cpn.getFosUserId());
            st.setString(3, Cpn.getLibelle());
            st.setString(4, Cpn.getDescription());
            st.setString(5, Cpn.getStatus());
            st.setString(6, Cpn.getTypec());
            st.setDate(7, Cpn.getDateceation());
            st.setDate(8, Cpn.getPeriodevalidite());
            st.setInt(9, id_coupon);
            st.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }

    }

    //afficher
    public ObservableList<Coupon> afficherCoupons() {
        ObservableList<Coupon> retour = FXCollections.observableArrayList();
        try {
            PreparedStatement pt = con.prepareStatement("SELECT * FROM coupon");
            ResultSet rs = pt.executeQuery();
            while (rs.next()) {
                int id_coupon = rs.getInt(1);
                int fos_user_id = rs.getInt(3);
                String libelle = rs.getString(4);
                String description = rs.getString(5);
                String status = rs.getString(6);
                String typec = rs.getString(7);
                Date dateceation = rs.getDate(8);
                Date periodevalidite = rs.getDate(9);
      
                retour.add(new Coupon(id_coupon, libelle, description, status, typec, dateceation, periodevalidite, fos_user_id));
            }
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }
        return retour;
    }

    //supprimer
    public void supprimerCoupon(int id) {
        try {
            PreparedStatement pt = con.prepareStatement("DELETE FROM coupon WHERE id_coupon = ?");
            pt.setInt(1, id);
            pt.execute();
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }
    }

}
