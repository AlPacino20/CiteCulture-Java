/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Fidelite;
import utilities.connexionBD;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author AlPacino20
 */
public class FideliteService {

    Connection con = connexionBD.getInstance().getConn();

    //create
    public void ajouterFidelite(Fidelite F) {
        try {
            Statement st = con.createStatement();
              String req="INSERT INTO  fidelite(`id_fidelite`, `fos_user_id`, `libelle`,`description`,`status`, `type` ,`dateobtenation`,`points`)  values('"+F.getIdFidelite()+"','"+F.getFosUserId()+"','"+F.getLibelle()+"','"+F.getDescription()+"','"+F.getStatus()+"','"+F.getType()+"','"+F.getDateobtenation()+"','"+F.getPoints()+"')";
            st.executeUpdate(req);
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }
    }
	
	
//modifier
    public void modifierFidelite(int id_Fidelite, Fidelite Fid) {
        try {
            PreparedStatement st = con.prepareStatement("update fidelite set id_fidelite=? , fos_user_id=? , libelle=? , description=? , status=? , type=? , dateobtenation=? , points=? where id_fidelite=?");

            st.setInt(1, Fid.getIdFidelite());
            st.setInt(2, Fid.getFosUserId());
            st.setString(3, Fid.getLibelle());
            st.setString(4, Fid.getDescription());
            st.setString(5, Fid.getStatus());
            st.setString(6, Fid.getType());
            st.setDate(7, Fid.getDateobtenation());
            st.setInt(8, Fid.getPoints());
            st.setInt(9, id_Fidelite);
            st.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }

    }

    //afficher
    public ObservableList<Fidelite> afficherFidelite() {
        ObservableList<Fidelite> retour = FXCollections.observableArrayList();
        try {
            PreparedStatement pt = con.prepareStatement("SELECT * FROM fidelite");
            ResultSet rs = pt.executeQuery();
            while (rs.next()) {
                int id_fidelite = rs.getInt(1);
                int fos_user_id = rs.getInt(2);
                String libelle = rs.getString(3);
                String description = rs.getString(4);
                String status = rs.getString(5);
                String type = rs.getString(6);
                Date dateobtenation = rs.getDate(7);
				int points = rs.getInt(8);
      
                retour.add(new Fidelite(id_fidelite, fos_user_id, libelle, description, status, type, dateobtenation, points));
            }
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }
        return retour;
    }

    //supprimer
    public void supprimerFidelite(int id) {
        try {
            PreparedStatement pt = con.prepareStatement("DELETE FROM fidelite WHERE id_fidelite = ?");
            pt.setInt(1, id);
            pt.execute();
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }
    }

}
